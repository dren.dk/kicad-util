package dk.dren.kicad.cmd;

import lombok.Getter;
import lombok.extern.java.Log;
import org.decimal4j.immutable.Decimal6f;
import picocli.CommandLine;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Level;

@Log
@Getter
@CommandLine.Command(name = "java -jar ku.jar", description = "The java based kicad file manipulation utility\n" +
        "Please RTFM before use: https://gitlab.com/dren.dk/kicad-util",
        subcommands = {PCBCmd.class})
public class Main implements Callable<Integer> {

    @CommandLine.Option(names = { "-h", "--help"}, usageHelp=true, description = "print usage help and exit")
    private boolean help;

    public static void main(String[] args) {
        // Note: This boilerplate is required because picocli exits with error code 0 on errors, counter to normal practice.
        try {
            Main self = new Main();
            final CommandLine commandLine = new CommandLine(self);

            commandLine.registerConverter(Decimal6f.class, Decimal6f::new);
            commandLine.registerConverter(CommaSeparatedIntegerList.class, CommaSeparatedIntegerList::new);

            final CommandLine.DefaultExceptionHandler<List<Object>> exceptionHandler = new CommandLine.DefaultExceptionHandler<>();
            exceptionHandler.andExit(254); // Any otherwise unhandled exception
            final List<Object> stuff = commandLine.parseWithHandlers(
                    new CommandLine.RunAll(),
                    exceptionHandler,
                    args);

            if (stuff == null) {
                System.exit(0);  // Help was run
            }

            if (stuff.size() < 2) {
                System.err.println("Error: Please supply a sub-command");
                CommandLine.usage(self, System.err); // No sub-command
                System.exit(0);
            }

            final Integer result = (Integer) stuff.get(stuff.size() - 1);
            System.exit(result);
        } catch (Throwable t) {
            log.log(Level.SEVERE, "Failed with command line: " + String.join(" ", args), t);
            System.err.println("Failed "+t+", see logs for details");
            System.exit(253); // Any exception not caught by the picocli exception handler
        }
    }

    @Override
    public Integer call() throws Exception {
       return 1;
    }
}
