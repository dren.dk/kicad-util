package dk.dren.kicad.textops;

import dk.dren.kicad.pcb.FpText;
import dk.dren.kicad.pcb.MutableValue;
import dk.dren.kicad.pcb.NodeOrValue;

public class Hide implements TextOperation {

    public static final int INDEX_OF_HIDE = 4;
    public static final String HIDE = "hide";
    public static final MutableValue MUTABLE_HIDE = new MutableValue(HIDE);

    @Override
    public void accept(FpText fpText) {
        fpText.alterChildren(ch->{
            NodeOrValue possiblyHide = ch.get(INDEX_OF_HIDE);
            if (possiblyHide instanceof MutableValue && ((MutableValue)possiblyHide).getValue().equals(HIDE)) {
                return;
            }

            ch.add(INDEX_OF_HIDE, MUTABLE_HIDE);
        });
    }
}
