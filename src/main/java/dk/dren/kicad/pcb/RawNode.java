package dk.dren.kicad.pcb;

import lombok.Getter;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This is the class of all nodes that do not have a more specific node implementation
 */
public class RawNode implements Node {
    @Getter
    private final String name;
    @Getter
    private List<NodeOrValue> allChildren;

    private List<MutableValue> attributes;
    private Map<String, List<Node>> properties;


    public RawNode(String name, NodeOrValue... allChildren) {
        this(name, new ArrayList<>(Arrays.asList(allChildren)));
    }

    public RawNode(String name, List<NodeOrValue> allChildren) {
        this.name = name;
        this.allChildren = allChildren;
    }

    public Node getProperty(String name) {
        return getFirstChildByName(name).orElse(null);
    }

    public Stream<Node> streamChildNodesByName(String name) {
        if (properties == null) {
            properties = new TreeMap<>();
            streamChildNodes().forEach(n->{
                properties.computeIfAbsent(n.getName(), nn->new ArrayList<>())
                        .add(n);
            });
        }

        List<Node> nodes = properties.get(name);
        if (nodes == null) {
            return Stream.empty();
        }

        return nodes.stream();
    }


    @Override
    public Optional<Node> getFirstChildByName(String name) {
        return streamChildNodesByName(name).findFirst();
    }

    public MutableValue getPropertyValue(String name) {
        return getProperty(name).getAttributes().get(0);
    }

    public List<MutableValue> getAttributes() {
        if (attributes == null) {
            attributes = streamAttributes().collect(Collectors.toList());
        }
        return attributes;
    }

    public boolean isKeyValue() {
        return getAllChildren().size() == 1 && getAllChildren().get(0) instanceof MutableValue;
    }

    @Override
    public String toString() {
        List<MutableValue> attributes = getAttributes();
        Collection<Node> children = getChildren();
        if (isKeyValue()) {
            return getName()+"="+ attributes.iterator().next();

        } else if (attributes.isEmpty()){
            return getName()+" ("+ children.size()+")";

        } else {
            return getName()+"=["+streamAttributes().map(MutableValue::toString).collect(Collectors.joining(" "))+"] ("+ children.size()+")";
        }
    }

    public void alterChildren(Consumer<List<NodeOrValue>> childConsumer) {
        childConsumer.accept(allChildren);
        attributes = null;
        properties = null;
    }

    public void addChild(Node child) {
        alterChildren(children-> children.add(child));
    }

    public void removeChild(NodeOrValue goner) {
        alterChildren(children-> children.remove(goner));
    }
}
