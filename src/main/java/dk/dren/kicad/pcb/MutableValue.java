package dk.dren.kicad.pcb;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.decimal4j.immutable.Decimal6f;

@AllArgsConstructor
@Data
public class MutableValue implements NodeOrValue {
    private String value;

    public MutableValue(Decimal6f v) {
        setValue(v);
    }

    @Override
    public String toString() {
        return value;
    }

    public Decimal6f getDecimal6f() {
        return new Decimal6f(value);
    }


    public void setValue(MutableValue v) {
        value = v.getValue();
    }

    public void setValue(Decimal6f v) {
        if (v.isZero()) {
            value = "0";

        } else if (v.isIntegral()) {
            value = Integer.toString(v.intValue());

        } else {
            String s = v.toString();
            if (s.contains(".")) {
                value = s.replaceAll("0+$", ""); // Get rid of decimals that aren't needed
            } else {
                value = s;
            }
        }
    }

    public void setValue(String value) {
        this.value = value;
    }
}
