package dk.dren.kicad.pcb;

import dk.dren.kicad.primitive.ModuleReference;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Delegate;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class Module implements Node {
    public static final String NAME = "module";
    @Delegate
    private final RawNode rawNode;
    private ModuleReference reference;

    public Position getPosition() {
        return (Position)getFirstChildByName(Position.AT).get();
    }

    public FpText getReferenceFpText() {
        return getFpText(FpText.REFERENCE).get();
    }

    public FpText getValueFpText() {
        return getFpText(FpText.VALUE).get();
    }

    public FpText getUserFpText() {
        return getFpText(FpText.USER).get();
    }

    public Optional<FpText> getFpText(String name) {
        return this.streamChildNodesByName(FpText.NAME)
                .map(n->(FpText)n).
                        filter(t->t.getTextKind().equals(name))
                .findFirst()
                ;
    }

    public ModuleReference getReference() {
        if (reference == null) {
            reference = new ModuleReference(getReferenceFpText().getTextValue());
        }
        return reference;
    }

    public String getValue() {
        return getValueFpText().getTextValue();
    }

    @Override
    public String toString() {
        return getReference()+" "+getValue()+" ("+getAttributes().get(0)+")";
    }

    /**
     * Clones some information from the template module to this module:
     * * The layer the module is on, which sadly, isn't enough.
     * * The layer of the reference and value texts
     * * The layer of lines
     * * The layer of pads
     * * The mirroring of texts
     * * The hide/show of texts
     * * The relative position of texts
     *
     * Such repeat, so redudancy, wow
     *
     * @param templateModule
     */
    public void cloneLayerAndStyle(Module templateModule) {
        getPropertyValue("layer").setValue(templateModule.getPropertyValue("layer"));
        getValueFpText().cloneStyle(templateModule.getValueFpText());
        getReferenceFpText().cloneStyle(templateModule.getReferenceFpText());
        getUserFpText().cloneStyle(templateModule.getUserFpText());

        cloneLineStyles(templateModule);
        clonePadLayers(templateModule);
    }

    private void clonePadLayers(Module templateModule) {
        Map<String, Node> templatePads = templateModule.getPads();
        Map<String, Node> pads = getPads();

        if (pads.size() != templatePads.size()) {
            throw new IllegalArgumentException("There should be the same number of pads in the target as the template pads: "+templateModule+" has "+templatePads.size()+" and "+this+" has "+pads.size());
        }

        for (Map.Entry<String, Node> idAndtemplatePad : templatePads.entrySet()) {
            RawNode targetPad = (RawNode)pads.get(idAndtemplatePad.getKey());
            if (targetPad == null) {
                throw new IllegalArgumentException("Cannot find target pad "+idAndtemplatePad.getKey()+" in "+this+" to match "+templateModule);
            }
            Node templateLayers = KicadPcbSerializer.clone(idAndtemplatePad.getValue().getFirstChildByName("layers").get());
            Node templateAt = KicadPcbSerializer.clone(idAndtemplatePad.getValue().getFirstChildByName("at").get());

            targetPad.alterChildren(children->{
                for (int ci=0; ci<children.size(); ci++) {
                    NodeOrValue child = children.get(ci);
                    if (child instanceof Node) {
                        Node childNode = (Node) child;
                        if (childNode.getName().equals("layers")) {
                            children.set(ci, templateLayers);
                        } else if (childNode.getName().equals("at")) {
                            children.set(ci, templateAt);
                        }
                    }
                }
            });
        }
    }

    public Map<String,Node> getPads() {
        Map<String, Node> padById = new TreeMap<>();

        streamChildNodesByName("pad").forEach(pad->{

            String id = pad.getAttributes().stream().map(MutableValue::getValue).collect(Collectors.joining("-"));
            //List<MutableValue> atat = pad.getFirstChildByName("at").get().getAttributes();
            //id += "@" + atat.get(0)+","+ atat.get(1);

            if (padById.containsKey(id)) {
                int conflictCount = 0;
                String conflictId = id;
                while (padById.containsKey(conflictId)) {
                    conflictId = id + conflictCount++;
                }
                padById.put(conflictId, pad);
            } else {
                padById.put(id, pad);
            }
        });

        return padById;
    }



    public List<Node> getLines() {
        return streamChildNodesByName("fp_line").collect(Collectors.toList());
    }

    public void cloneLineStyles(Module templateModule) {
        List<Node> templateLines = templateModule.getLines();
        List<Node> myLines = getLines();

        if (myLines.size() != templateLines.size()) {
            throw new IllegalArgumentException("There should be the same number of lines in the target as the template lines: "+templateModule+" has "+templateLines.size()+" and "+this+" has "+myLines.size());
        }

        for (int i = 0; i < templateLines.size(); i++) {
            myLines.get(i).getFirstChildByName("layer").get().getAttributes().get(0).setValue(
                    templateLines.get(i).getFirstChildByName("layer").get().getAttributes().get(0)
            );
        }
    }
}
