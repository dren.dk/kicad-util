package dk.dren.kicad.pcb;

import org.decimal4j.immutable.Decimal6f;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class VectorTest {

    @Test
    public void length() {
        Vector v = new Vector(30,40);
        Assert.assertEquals(50, v.length().intValue());
    }

    @Test
    public void unit() {
        Vector v = new Vector(47,100);
        Assert.assertEquals(1.0, v.unit().length().doubleValue(), 0.001);
    }

    @Test
    public void normal() {
        Vector v = new Vector(10, 5);
        Vector normal = v.normal();
        Assert.assertEquals(new Vector(5, -10), normal);
    }

    @Test
    public void dot() {
        Vector v = new Vector(10, 5).unit();
        Vector normal = v.normal();
        Assert.assertEquals(0, v.dot(normal), 0.001);
        Assert.assertEquals(1, v.dot(v), 0.001);
        Assert.assertEquals(-1, v.dot(v.multiply(Decimal6f.valueOf(-1))), 0.001);
    }

    @Test
    public void angle() {
        Vector v = new Vector(10, 5);
        Vector normal = v.normal().multiply(Decimal6f.valueOf(10));

        Assert.assertEquals(Math.PI/2, v.angle(normal), 0.0001);
    }

    @Test
    public void angle2() {
        Vector v = new Vector(1, 0);
        Assert.assertEquals(0, v.angle(), 0.0001);

        for (int i=-499;i<499;i++) {
            double a = i*2*Math.PI/1000;
            Vector rotated = v.rotate(a);
            Assert.assertEquals("Failed to rotate vector "+a+" at step "+i+" rotated vector: "+rotated, a, rotated.angle(), 0.0001);
        }
    }
}